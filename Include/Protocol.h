

#ifndef __PROTOCOL_H
#define __PROTOCOL_H
#ifdef __cplusplus
 extern "C" {
#endif
		
		#include <stdint.h>
		#include "Process_Control.h"
		#include "Protocol_PackageDefine.h"
		
		/* Protocol Define */
		#define ErrTimeout_Time 16 /* x10ms */
		#define ErrTimeout_Cnt	5
		
		/* Package Compare Define */
		#define PackageHeaderCompareLength	3
		#define VCU_ReqHeader 	{ ohVCU, hRequset, hRequset }
		#define VCU_RspHeader		{ ohVCU, hResponse, hResponse }
		#define VCU_DataHeader	{ ohVCU, hRequset, ctWriteData }
		
		/* Protocol Information*/
		typedef struct
		{
			uint32_t PackageSize;
			uint32_t TotalBinSize;
			uint32_t PackageQuantity;
			uint32_t PackageNumCnt;
		}PROTOCOLINFORMATION;
		
		uint8_t ProtocolExamination(BOOTPROCESS_PROCESS *p_Process, PERIPHERALDATA *p_Data);
		
		static uint8_t PackageHeaderCompare(uint8_t *ControlGroup, uint8_t *TestGroup, uint8_t length);
		uint8_t * Protocol_AppFwUpdateTime_Read(void);

#ifdef __cplusplus
}
#endif
#endif /*__PROTOCOL_H */
