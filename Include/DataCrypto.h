

#ifndef __DATACRYPTO_H
#define __DATACRYPTO_H
#ifdef __cplusplus
 extern "C" {
#endif
		
		#include <stdint.h>
		
		#define NoCryto					0x00
		#define EasyCrypto			0x01
		

		void CryptoProcess(uint8_t InputBuf[], uint8_t Datalength, uint8_t OutputBuf[]);

#ifdef __cplusplus
}
#endif
#endif /*__DATACRYPTO_H */
