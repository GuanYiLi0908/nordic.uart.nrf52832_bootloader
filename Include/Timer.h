

#ifndef __TIMER_H
#define __TIMER_H
#ifdef __cplusplus
 extern "C" {
#endif
		
		#include <stdint.h>
		#include "nrf.h"
		#include "nrf_drv_timer.h"
		
		void Timer_Initialzation(void);
		void timer_led_event_handler(nrf_timer_event_t event_type, void* p_context);
		


#ifdef __cplusplus
}
#endif
#endif /*__TIMER_H */
