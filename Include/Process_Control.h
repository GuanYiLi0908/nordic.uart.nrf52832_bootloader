

#ifndef __PROCESS_CONTROL_H
#define __PROCESS_CONTROL_H
#ifdef __cplusplus
 extern "C" {
#endif
		
		#include <stdint.h>
		
//		#define forUARTDataNRF_LOG	1
//		#define forMemWriteNRF_LOG 1
//		#define forMemReadNRF_LOG 1
		
		/* Flag */
		typedef struct
		{
			uint8_t UARTReceiveFinish : 1;
		}PERIPHERALFLAG;
		
		#define TransmitStatus		0
		#define ReceiveStatus			1
		typedef struct
		{
			uint8_t Connection			: 1;
			uint8_t ErrorProcessing	: 1;
		}BOOTPROCESS_FLAG;
		
		/* Main Process */
		typedef enum
		{
			Connection = 0,
			ReceiveSetting,
			FirmwareReceive,
			Activation,
			ErrorProcessing,
		}MAINPROCESS_STATUS;
		
		/* Memory Operation Process */
		typedef enum
		{
			MemProgramInfoCal = 0,
			MemoryWrite,
		}MEMORYOPERATIONPROCESS_STATUS;
		
		typedef struct
		{
			MAINPROCESS_STATUS MainProcess;
			MAINPROCESS_STATUS PreviousProcess;
			MEMORYOPERATIONPROCESS_STATUS MemOperationProcess;
		}BOOTPROCESS_PROCESS;

#ifdef __cplusplus
}
#endif
#endif /*__PROCESS_CONTROL_H */
