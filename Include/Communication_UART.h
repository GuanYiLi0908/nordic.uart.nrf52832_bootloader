

#ifndef __COMMUNICATION_UART_H
#define __COMMUNICATION_UART_H
#ifdef __cplusplus
 extern "C" {
#endif
		
		#include <stdint.h>
		#include "nrf_drv_uart.h"
		#include "app_uart.h"
		#include "app_fifo.h"
		#include "Process_Control.h"
		#include "Protocol_PackageDefine.h"
		
		
		/* UART */
		#define UART_TX_BUF_SIZE          					256                                   /**< UART TX buffer size. */
		#define UART_RX_BUF_SIZE          					256                                   /**< UART RX buffer size. */
		#define DATA_CRC32_LENGTH										4			

		typedef struct
		{
			uint8_t ByteCounter;
			uint8_t Data[CommandPackageLength - UnusedPackageLength];
		}UARTRECEIVE;
		
		void UART_Initialization(void);
		static void UARTEvent_handler(app_uart_evt_t * p_event);
		void UARTDataTransimt(uint8_t *p_Data, uint16_t DataLength);

#ifdef __cplusplus
}
#endif
#endif /*__COMMUNICATION_UART_H */
