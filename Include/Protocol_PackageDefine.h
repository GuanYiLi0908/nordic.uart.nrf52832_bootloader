

#ifndef __PROTOCOL_PACKAGEDEFINE_H
#define __PROTOCOL_PACKAGEDEFINE_H
#ifdef __cplusplus
 extern "C" {
#endif
		
		#include <stdint.h>
		
		/* Package Length */
		#define CommandPackageLength 	27
		#define DataPackageLength 		27
		#define UnusedPackageLength		0
		
		/* OBD Header */
		#define ohVCU							0x1A		/* VCU -> BLE */
		#define ohBluetooth				0xBE		/* BLE -> VCU */
		
		/* Bootloader Header */
		#define hNotifyBoot				0x80
		#define hRequset					0x0F
		#define hResponse					0xF0
		
		/* Command Type */
		#define ctExit						0x02
		#define ctNotifyBoot			0x0E
		
		#define ctPackageSize			0x10
		#define ctStartAndSetting	0x11
		
		#define ctWriteData				0x20
		
		#define ctCRCreq					0x30
		#define ctChangeKey				0x31
		#define ctTransmitKey			0x32
		#define ctCRCrsp					0x3F
		
		#define ctACK							0xE0
		#define ctBusy						0xE1
		#define ctError						0xE2
		
		/* Error Code */
		#define ePackageSize			0x01
		#define eCRCType					0x02
		#define eCRCValue					0x03
		#define eInvalidValue			0x04
		#define eUnknowCommand		0x05
		#define eTimeout					0x06
		#define eMemoryFull				0x07
		
		/* Data */
		typedef struct
		{
			uint8_t cUARTRx[CommandPackageLength - UnusedPackageLength];
		}PERIPHERALDATA;
		
		/* NotifyBoot */
		typedef struct
		{
			uint8_t OBD_Header;
			uint8_t OBD_Length;
			uint8_t Header;
			uint8_t CommandType;
			uint8_t ID[2];
			uint8_t DeviceType;
			uint8_t UserReserve[2];
			uint8_t BootProtocolVer;
			uint8_t BootFwVer;
			uint8_t Reserve[CommandPackageLength - 11];
		}NOTIFYBOOT;	
		
		/* ACK */
		typedef struct
		{
			uint8_t OBD_Header;
			uint8_t OBD_Length;
			uint8_t Header;
			uint8_t CommandType;
			uint8_t ContinueTransCnt;
			uint8_t BootPkgReserve[2];
			uint8_t AppFwCRC[4];
			uint8_t Reserve[CommandPackageLength - 11];
		}ACK;	
		
		/* Error */
		typedef struct
		{
			uint8_t OBD_Header;
			uint8_t OBD_Length;
			uint8_t Header;
			uint8_t CommandType;
			uint8_t ErrorCode;
			uint8_t Reserve[CommandPackageLength - 5];
		}ERROR;
		
		/* Exit */
		typedef struct
		{
			uint8_t OBD_Header;
			uint8_t OBD_Length;
			uint8_t Header;
			uint8_t CommandType;
			uint8_t BootStatus;
			uint8_t Time[6];
			uint8_t Reserve[CommandPackageLength - 11];
		}EXIT;
		
		/* Bootloader Information Reserve Area(0x72000, 256/512) */
		#define BootReserveAreaPageAddr		0x72000
		#define BootReserveAreaAddr				0x72E00
		#define BootReserveAreaLength			28
		typedef struct
		{
			/* Bootloader Information */	
			uint8_t BootFwVer[2];
			uint8_t BootProtocolVer;							
			uint8_t BootReserve;
			uint32_t BootFwBinCRC;
			
			/* Application Fw Information */
			uint8_t AppReserve[2];
			uint8_t PreviousUpdateTime[6];			
			uint32_t AppFwBinSize;
			uint32_t AppFwBinCRC;
			
			/* Reserve Area Information */
			uint32_t ReserveAreaCRC;
		}BOOTINFORESERVEAREA;


#ifdef __cplusplus
}
#endif
#endif /*__PROTOCOL_PACKAGEDEFINE_H */
