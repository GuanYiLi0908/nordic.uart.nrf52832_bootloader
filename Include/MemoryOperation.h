

#ifndef __BOOTLOADEROPERATION_H
#define __BOOTLOADEROPERATION_H
#ifdef __cplusplus
 extern "C" {
#endif
		
		#include <stdint.h>
		#include "sdk_errors.h"
		#include "nrf_dfu_types.h"
		#include "nrf_fstorage.h"
		#include "nrf_fstorage_nvmc.h"
		#include "nrf_bootloader_info.h"
		#include "Process_Control.h"

//		#define forMemWriteNRF_LOG					1		
//		#define forMemReadNRF_LOG						1
//		#define forMemOperationNRF_LOG			1
		
		#define FwStartAddress						0x26000
		
		#define FstorageProgramUnit 4
		#define FwProgramPoolSize	128
		
		typedef struct 
		{
			uint32_t ProgramAddress;
			uint8_t UncutMaxLen;
			uint8_t FirmwareProgramPool[FwProgramPoolSize];
			uint8_t PoolUsedLength;
		}FIRMWAREPROGRAMINFOMATION;

		void Process_MemoryOperation(BOOTPROCESS_PROCESS *p_Process);
		uint32_t MemoryOperation_AppFwCRC_Read(void);
		
		void MemoryOperation_BootInfo_Calculate(void);
		void MemoryOperation_BootInfo_Write(void);
		void MemoryOperation_BootInfo_Read(void);
		
		void MemoryOperation_Initialization(void);
		void fstorageEvent_handler(nrf_fstorage_evt_t * p_evt);
		
/**@brief   nrf_fstorage event handler function for DFU fstorage operations.
 *
 * This function will be called after a flash operation has completed.
 */
typedef void (*nrf_dfu_flash_callback_t)(void * p_buf);		
		
/**@brief Function for storing data to flash.
 *
 * This functions is asynchronous when the SoftDevice is enabled and synchronous when
 * the SoftDevice is not present or disabled. In both cases, if a callback function is provided,
 * it will be called when the operation has completed.
 *
 * @note The content of @p p_src should be kept in memory until the operation has completed.
 *
 * @param[in]  dest      The address where the data should be stored.
 * @param[in]  p_src     Pointer to the address where the data should be copied from.
 *                       This address can be in flash or RAM.
 * @param[in]  len       The number of bytes to be copied from @p p_src to @p dest.
 * @param[in]  callback  Callback function.
 *
 * @retval  NRF_SUCCESS                 If the operation was successful.
 * @retval  NRF_ERROR_INVALID_STATE     If nrf_dfu_flash is not initialized.
 * @retval  NRF_ERROR_INVALID_ADDR      If @p p_src or @p dest is not word-aligned.
 * @retval  NRF_ERROR_INVALID_LENGTH    If @p len is zero.
 * @retval  NRF_ERROR_NULL              If @p p_src is NULL.
 * @retval  NRF_ERROR_NO_MEM            If nrf_fstorage is out of memory.
 */
ret_code_t nrf_dfu_flash_store(uint32_t                     dest,
                               void                 const * p_src,
                               uint32_t                     len,
                               nrf_dfu_flash_callback_t     callback);


/**@brief Function for erasing data from flash.
 *
 * This functions is asynchronous when the SoftDevice is enabled and synchronous when
 * the SoftDevice is not present or disabled. In both cases, if a callback function is provided,
 * it will be called when the operation has completed.
 *
 * @param[in]  page_addr    The address of the first flash page to be deleted.
 * @param[in]  num_pages    The number of flash pages to be deleted.
 * @param[in]  callback     Callback function.
 *
 * @retval  NRF_SUCCESS                 If the operation was successful.
 * @retval  NRF_ERROR_INVALID_STATE     If nrf_dfu_flash is not initialized.
 * @retval  NRF_ERROR_INVALID_ADDR      If @p page_addr is not aligned to a page boundary or the
 *                                      operation would go beyond the flash memory boundaries.
 * @retval  NRF_ERROR_INVALID_LENGTH    If @p num_pages is zero.
 * @retval  NRF_ERROR_NULL              If @p page_addr is NULL.
 * @retval  NRF_ERROR_NO_MEM            If the queue of nrf_fstorage is full.
 */
ret_code_t nrf_dfu_flash_erase(uint32_t page_addr, uint32_t num_pages, nrf_dfu_flash_callback_t callback);		

ret_code_t nrf_dfu_flash_read(uint32_t read_addr, uint32_t length, void *buf);
		
/**
 * Round up val to the next page boundary
 */
#define ALIGN_TO_PAGE(val) ALIGN_NUM((CODE_PAGE_SIZE), (val))


/** @brief Function for getting the start address of bank 0.
 *
 * @note Bank 0 starts after the SoftDevice if a SoftDevice is present.
 *
 * @return The start address of bank 0.
 */
uint32_t nrf_dfu_bank0_start_addr(void);


/** @brief Function for getting the start address of bank 1.
 *
 * @return The start address of bank 1.
 */
uint32_t nrf_dfu_bank1_start_addr(void);


/** @brief Function for getting the start address of the app.
 *
 * @return  The start address of the bootable app.
 */
uint32_t nrf_dfu_app_start_address(void);


/** @brief Function for getting the start address of the SoftDevice.
 *
 * @return  The start address of the SoftDevivce.
 */
uint32_t nrf_dfu_softdevice_start_address(void);

extern uint8_t * BootloaderVersionRead(void);
extern uint8_t BootloaderProtocolVersionRead(void);

#ifdef __cplusplus
}
#endif
#endif /*__BOOTLOADEROPERATION_H */
