

#include <stdint.h>
#include "Timer.h"
#include "bsp.h"

const nrf_drv_timer_t MainProcess_Timer = NRF_DRV_TIMER_INSTANCE(0);
uint32_t Timer_BasicTick_Cnt = 0;

/******************************************************************************/
/*                 	  			Timer Function subroutine		                	  	*/
/******************************************************************************/
void Timer_Initialzation(void)
{
	ret_code_t err_code;
	uint32_t time_ms = 10;
	uint32_t time_ticks = 0;
	
	nrf_drv_timer_config_t timer_cfg = NRF_DRV_TIMER_DEFAULT_CONFIG;
	
	err_code = nrf_drv_timer_init(&MainProcess_Timer, &timer_cfg, timer_led_event_handler);
  APP_ERROR_CHECK(err_code);
	
	time_ticks = nrf_drv_timer_ms_to_ticks(&MainProcess_Timer, time_ms);

  nrf_drv_timer_extended_compare(&MainProcess_Timer, NRF_TIMER_CC_CHANNEL0, time_ticks, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, true);

  nrf_drv_timer_enable(&MainProcess_Timer);
}

/**
 * @brief Handler for timer events.
 */
void timer_led_event_handler(nrf_timer_event_t event_type, void* p_context)
{
    static uint32_t i;
    uint32_t led_to_invert = ((i++) % LEDS_NUMBER);

    switch (event_type)
    {
        case NRF_TIMER_EVENT_COMPARE0:
						Timer_BasicTick_Cnt++;
            bsp_board_led_invert(led_to_invert);
            break;

        default:
            //Do nothing.
            break;
    }
}
