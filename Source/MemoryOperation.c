

#include "sdk_errors.h"
#include "crc32.h"
#include "Protocol.h"
#include "Process_Control.h"
#include "Protocol_PackageDefine.h"
#include "DataCrypto.h"
#include "MemoryOperation.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

nrf_dfu_settings_t s_dfu_settings;
FIRMWAREPROGRAMINFOMATION sFwProgramInfo;

extern BOOTPROCESS_PROCESS sBootloader;
extern BOOTINFORESERVEAREA sBootInfoArea;
extern PROTOCOLINFORMATION sProtocolInfo;
extern PERIPHERALDATA sPeripheralData;

NRF_FSTORAGE_DEF(nrf_fstorage_t m_fs) =
{
    .evt_handler = fstorageEvent_handler,
    .start_addr  = MBR_SIZE,
    .end_addr    = BOOTLOADER_SETTINGS_ADDRESS + BOOTLOADER_SETTINGS_PAGE_SIZE
};

static uint32_t m_flash_operations_pending;
static uint32_t NewAppFwCRC_Buf = 0;

/******************************************************************************/
/*                 	  		 Memory Operation Process				              	  	*/
/******************************************************************************/
void Process_MemoryOperation(BOOTPROCESS_PROCESS *p_Process)
{				
	ret_code_t err_code;
	
	switch(p_Process -> MainProcess)
	{
		case Connection:
			NRF_LOG_INFO("Memory Program Information Calculate");
		
			sFwProgramInfo.ProgramAddress = FwStartAddress;
		
			/* No need to cut maximum length */
			sFwProgramInfo.UncutMaxLen = FwProgramPoolSize - (FwProgramPoolSize % sProtocolInfo.PackageSize);
			NRF_LOG_DEBUG("sFwProgramInfo.UncutMaxLen : %d", sFwProgramInfo.UncutMaxLen);

			/* Erase All Firmware and FirmwareProgramPool */
			memset(sFwProgramInfo.FirmwareProgramPool, 0, FwProgramPoolSize);
		
			err_code = nrf_fstorage_erase(&m_fs, sFwProgramInfo.ProgramAddress, 70, NULL);
			if(err_code != NRF_SUCCESS)
			{
				NRF_LOG_ERROR("nrf_fstorage_erase Application Error : %d", err_code);
			}
			
			/* Erase Bootloader Information Reserve Area */
			err_code = nrf_fstorage_erase(&m_fs, BootReserveAreaPageAddr, 1, NULL);
			if(err_code != NRF_SUCCESS)
			{
				NRF_LOG_ERROR("nrf_fstorage_erase BootReserveArea Error : %d", err_code);
			}
			break;
		
		case FirmwareReceive:		
			if(sFwProgramInfo.PoolUsedLength < sFwProgramInfo.UncutMaxLen)
			{
				memcpy(sFwProgramInfo.FirmwareProgramPool + sFwProgramInfo.PoolUsedLength, 
							 sPeripheralData.cUARTRx + 4, 
							 sProtocolInfo.PackageSize);
				sFwProgramInfo.PoolUsedLength = sFwProgramInfo.PoolUsedLength + sProtocolInfo.PackageSize;
			}
			else
			{
				uint8_t tempPackageCutLength = FwProgramPoolSize - sFwProgramInfo.UncutMaxLen;
				memcpy(sFwProgramInfo.FirmwareProgramPool + sFwProgramInfo.PoolUsedLength, 
							 sPeripheralData.cUARTRx + 4, 
							 tempPackageCutLength);
				
				uint8_t tempRemainderLength = sProtocolInfo.PackageSize - tempPackageCutLength;
				uint8_t *tempRemainderData_Array = malloc(tempRemainderLength * sizeof(uint8_t));
				memcpy(tempRemainderData_Array, sPeripheralData.cUARTRx + 4 + tempPackageCutLength, tempRemainderLength);
				
				/* Memory Program */
//				NRF_LOG_DEBUG("ProgramAddress : 0x%08X", sFwProgramInfo.ProgramAddress);
				
#ifdef	forMemWriteNRF_LOG
				NRF_LOG_RAW_INFO("FirmwareProgramPool :");
				for(uint8_t i = 0; i < FwProgramPoolSize; i++)
				{
					NRF_LOG_RAW_INFO(" %02X ", sFwProgramInfo.FirmwareProgramPool[i]);
				}
				NRF_LOG_RAW_INFO("\r\n");
#endif				
				
				uint8_t temp[FwProgramPoolSize] = { 0 };
				CryptoProcess(sFwProgramInfo.FirmwareProgramPool, FwProgramPoolSize, temp);
//				memcpy(temp, sFwProgramInfo.FirmwareProgramPool, FwProgramPoolSize);
				err_code = nrf_fstorage_write(&m_fs, 
																			sFwProgramInfo.ProgramAddress, 
																			temp,
																			FwProgramPoolSize, 
																			NULL);
				if(err_code != NRF_SUCCESS)
				{
					NRF_LOG_ERROR("nrf_fstorage_write Error : %d", err_code);
				}
				
				/* While fstorage is busy, sleep and wait for an event. */
				while (nrf_fstorage_is_busy(&m_fs))
				{
					;
				}
				
#ifdef forMemReadNRF_LOG
				uint8_t tempRead_Array[FwProgramPoolSize] = { 0 };
				err_code = nrf_fstorage_read(&m_fs, 
																		 sFwProgramInfo.ProgramAddress, 
																		 tempRead_Array, 
																		 FwProgramPoolSize);

				if(err_code != NRF_SUCCESS)
				{
					NRF_LOG_INFO("nrf_fstorage_read Error : %d", err_code);
				}
			
				NRF_LOG_RAW_INFO("tempRead_Array :");
				for(uint8_t i = 0; i < FwProgramPoolSize; i++)
				{
					NRF_LOG_RAW_INFO(" %02X ", tempRead_Array[i]);
				}
				NRF_LOG_RAW_INFO("\r\n");				
#endif
				
				/* Clean Pool and loading remainder data */
				memset(sFwProgramInfo.FirmwareProgramPool, 0, FwProgramPoolSize);
				memcpy(sFwProgramInfo.FirmwareProgramPool, tempRemainderData_Array, tempRemainderLength);
				free(tempRemainderData_Array);
				
				/* ReCalculate Parameter */
				sFwProgramInfo.PoolUsedLength = tempRemainderLength;
				sFwProgramInfo.UncutMaxLen = FwProgramPoolSize - ((FwProgramPoolSize - tempRemainderLength) % sProtocolInfo.PackageSize);
//				NRF_LOG_DEBUG("sFwProgramInfo.UncutMaxLen : %d", sFwProgramInfo.UncutMaxLen);
				
				sFwProgramInfo.ProgramAddress = sFwProgramInfo.ProgramAddress + FwProgramPoolSize;
			}

			/* Remained Data Process */
			if(sProtocolInfo.PackageNumCnt == sProtocolInfo.PackageQuantity)
			{
				NRF_LOG_INFO("Program all data who remained in the pool : 0x%08X", sFwProgramInfo.ProgramAddress);
				if((sFwProgramInfo.PoolUsedLength % FstorageProgramUnit) == 0)
				{
					uint8_t temp[FwProgramPoolSize] = { 0 };
					CryptoProcess(sFwProgramInfo.FirmwareProgramPool, sFwProgramInfo.PoolUsedLength, temp);
//					memcpy(temp, sFwProgramInfo.FirmwareProgramPool, sFwProgramInfo.PoolUsedLength);
					err_code = nrf_fstorage_write(&m_fs, 
																				sFwProgramInfo.ProgramAddress, 
																				temp,
																				sFwProgramInfo.PoolUsedLength, 
																				NULL);
					if(err_code != NRF_SUCCESS)
					{
						NRF_LOG_ERROR("nrf_fstorage_write Error : %d", err_code);
					}
				
					/* While fstorage is busy, sleep and wait for an event. */
					while (nrf_fstorage_is_busy(&m_fs))
					{
						;
					}
				}
				else
				{
					/* Add memory program length to let it divisible */
					uint8_t DivisibleLen_Buf = sFwProgramInfo.PoolUsedLength;
					NRF_LOG_DEBUG(" Before add length : %d ", sFwProgramInfo.PoolUsedLength); 
					
					do
					{
						DivisibleLen_Buf = DivisibleLen_Buf + 1;
					}while((DivisibleLen_Buf % FstorageProgramUnit) != 0);
					NRF_LOG_DEBUG(" DivisibleLen_Buf : %d ", DivisibleLen_Buf); 
						
					uint8_t temp[FwProgramPoolSize] = { 0 };
//					memcpy(temp, sFwProgramInfo.FirmwareProgramPool, sFwProgramInfo.PoolUsedLength);
					CryptoProcess(sFwProgramInfo.FirmwareProgramPool, sFwProgramInfo.PoolUsedLength, temp);
					err_code = nrf_fstorage_write(&m_fs, 
																				sFwProgramInfo.ProgramAddress, 
																				temp,
																				DivisibleLen_Buf, 
																				NULL);
					if(err_code != NRF_SUCCESS)
					{
						NRF_LOG_ERROR("nrf_fstorage_write Error : %d", err_code);
					}
				
					/* While fstorage is busy, sleep and wait for an event. */
					while (nrf_fstorage_is_busy(&m_fs))
					{
						;
					}
				}
				
				/* New Application Firmware CRC Calculate */
				NewAppFwCRC_Buf = crc32_compute((uint8_t *)FwStartAddress, sProtocolInfo.TotalBinSize, NULL);
				NRF_LOG_INFO("New Application Firmware CRC : %X ",NewAppFwCRC_Buf);
			}
			break;
		
		default:
			break;
	}
}

uint32_t MemoryOperation_AppFwCRC_Read(void)
{
	return NewAppFwCRC_Buf;
}

void MemoryOperation_BootInfo_Calculate(void)
{	
	uint8_t *p_FwVer;
	
	/* Update Fw Version */
	p_FwVer = BootloaderVersionRead();
	sBootInfoArea.BootFwVer[0] = *(p_FwVer + 0);
	sBootInfoArea.BootFwVer[1] = *(p_FwVer + 1);
	
	sBootInfoArea.BootProtocolVer = BootloaderProtocolVersionRead();
	
	/* Bootloader Firmware CRC Calculate */
	sBootInfoArea.BootFwBinCRC = crc32_compute((uint8_t *)0x73000,
																						0xB000,
																						NULL);
	
	/* New Application Firmware Update Time */
	uint8_t *p_UpdateTime = Protocol_AppFwUpdateTime_Read();
	for(uint8_t i = 0; i < 6; i++)
	{
		sBootInfoArea.PreviousUpdateTime[i] = *(p_UpdateTime + i);
	}
	
	/* New Application Firmware Size */
	sBootInfoArea.AppFwBinSize = sProtocolInfo.TotalBinSize;
	
	/* New Application Firmware CRC Calculate */
	sBootInfoArea.AppFwBinCRC = NewAppFwCRC_Buf;
	
	/* Bootloader Information Reserve Area CRC Calculate */
	uint8_t tempBootInfo_Ary[BootReserveAreaLength] = { 0 };
	memcpy(tempBootInfo_Ary, &sBootInfoArea, BootReserveAreaLength - sizeof(uint32_t));
	
#ifdef forMemOperationNRF_LOG	
	NRF_LOG_DEBUG("tempBootInfo_Ary :");
	for(uint8_t i = 0; i < (BootReserveAreaLength - sizeof(uint32_t)); i++)
	{
		NRF_LOG_RAW_INFO(" %02X ", tempBootInfo_Ary[i]);
	}
	NRF_LOG_RAW_INFO("\r\n");
#endif	
	
	sBootInfoArea.ReserveAreaCRC = crc32_compute((uint8_t *)tempBootInfo_Ary,
																							 BootReserveAreaLength - sizeof(uint32_t),
																							 NULL);																							 
//	NRF_LOG_DEBUG("sBootInfoArea.ReserveAreaCRC : %X ", sBootInfoArea.ReserveAreaCRC);
}

void MemoryOperation_BootInfo_Write(void)
{
	ret_code_t err_code;
	uint8_t tempBootInfo[BootReserveAreaLength] = { 0 };
	memcpy(tempBootInfo, &sBootInfoArea, BootReserveAreaLength);
	
	err_code = nrf_fstorage_erase(&m_fs, BootReserveAreaPageAddr, 1, NULL);
	if(err_code != NRF_SUCCESS)
	{
		NRF_LOG_ERROR("nrf_fstorage_erase Error : %d", err_code);
	}
	
	err_code = nrf_fstorage_write(&m_fs, 
																BootReserveAreaAddr, 
																tempBootInfo,
																BootReserveAreaLength, 
																NULL);
	if(err_code != NRF_SUCCESS)
	{
		NRF_LOG_ERROR("nrf_fstorage_write Error : %d", err_code);
	}
	
	while (nrf_fstorage_is_busy(&m_fs))
	{
		;
	}
}

void MemoryOperation_BootInfo_Read(void)
{
	uint8_t tempBootInfo[BootReserveAreaLength] = { 0 };
		
	nrf_fstorage_read(&m_fs, BootReserveAreaAddr, tempBootInfo, (uint32_t)BootReserveAreaLength);		
	
	memset(&sBootInfoArea, 0, sizeof(sBootInfoArea));
	memcpy(&sBootInfoArea, tempBootInfo, BootReserveAreaLength);
	
	NRF_LOG_RAW_INFO("Bootloader Firmware Version : %02X.%02X \r\n", sBootInfoArea.BootFwVer[0], sBootInfoArea.BootFwVer[1]);	
	NRF_LOG_RAW_INFO("Bootloader Protocol Version : %d \r\n", sBootInfoArea.BootProtocolVer);	
	NRF_LOG_RAW_INFO("Bootloader Firmware Bin CRC : %X \r\n", sBootInfoArea.BootFwBinCRC);
		
	NRF_LOG_RAW_INFO("Application Previous Update Time : %d%d%d.%d%d%d \r\n", sBootInfoArea.PreviousUpdateTime[0],
																																						sBootInfoArea.PreviousUpdateTime[1],
																																						sBootInfoArea.PreviousUpdateTime[2],
																																						sBootInfoArea.PreviousUpdateTime[3],
																																						sBootInfoArea.PreviousUpdateTime[4],
																																						sBootInfoArea.PreviousUpdateTime[5]);
																																	
	NRF_LOG_RAW_INFO("Application Firmware Size : %X Bytes \r\n", sBootInfoArea.AppFwBinSize);		
	NRF_LOG_RAW_INFO("Application Firmware CRC : %X \r\n", sBootInfoArea.AppFwBinCRC);
																																						
	NRF_LOG_RAW_INFO("Reserve Area CRC : %X \r\n", sBootInfoArea.ReserveAreaCRC);	
}
/******************************************************************************/
/*                 	 	Memory Operation Initialization			              	  	*/
/******************************************************************************/
void MemoryOperation_Initialization(void)
{
	ret_code_t err_code;
	nrf_fstorage_api_t * p_api_impl;
  
	p_api_impl = &nrf_fstorage_nvmc;
	err_code = nrf_fstorage_init(&m_fs, p_api_impl, NULL);
	APP_ERROR_CHECK(err_code);
}

void fstorageEvent_handler(nrf_fstorage_evt_t * p_evt)
{
    if (NRF_LOG_ENABLED && (m_flash_operations_pending > 0))
    {
        m_flash_operations_pending--;
    }

    if (p_evt->result == NRF_SUCCESS)
    {
        NRF_LOG_DEBUG("Flash %s success: addr=%p, pending %d",
                      (p_evt->id == NRF_FSTORAGE_EVT_WRITE_RESULT) ? "write" : "erase",
                      p_evt->addr, m_flash_operations_pending);
    }
    else
    {
        NRF_LOG_DEBUG("Flash %s failed (0x%x): addr=%p, len=0x%x bytes, pending %d",
                      (p_evt->id == NRF_FSTORAGE_EVT_WRITE_RESULT) ? "write" : "erase",
                      p_evt->result, p_evt->addr, p_evt->len, m_flash_operations_pending);
    }

    if (p_evt->p_param)
    {
        //lint -save -e611 (Suspicious cast)
        ((nrf_dfu_flash_callback_t)(p_evt->p_param))((void*)p_evt->p_src);
        //lint -restore
    }
}

/******************************************************************************/
/*                 	  		Memory Operation subroutine		                	  	*/
/******************************************************************************/
ret_code_t nrf_dfu_flash_store(uint32_t                   dest,
                               void               const * p_src,
                               uint32_t                   len,
                               nrf_dfu_flash_callback_t   callback)
{
    ret_code_t rc;

    NRF_LOG_DEBUG("nrf_fstorage_write(addr=%p, src=%p, len=%d bytes), queue usage: %d",
                  dest, p_src, len, m_flash_operations_pending);

    //lint -save -e611 (Suspicious cast)
    rc = nrf_fstorage_write(&m_fs, dest, p_src, len, (void *)callback);
    //lint -restore

    if ((NRF_LOG_ENABLED) && (rc == NRF_SUCCESS))
    {
        m_flash_operations_pending++;
    }
    else
    {
        NRF_LOG_WARNING("nrf_fstorage_write() failed with error 0x%x.", rc);
    }

    return rc;
}

ret_code_t nrf_dfu_flash_erase(uint32_t                 page_addr,
                               uint32_t                 num_pages,
                               nrf_dfu_flash_callback_t callback)
{
    ret_code_t rc;

    NRF_LOG_DEBUG("nrf_fstorage_erase(addr=0x%p, len=%d pages), queue usage: %d",
                  page_addr, num_pages, m_flash_operations_pending);

    //lint -save -e611 (Suspicious cast)
    rc = nrf_fstorage_erase(&m_fs, page_addr, num_pages, (void *)callback);
    //lint -restore

    if ((NRF_LOG_ENABLED) && (rc == NRF_SUCCESS))
    {
        m_flash_operations_pending++;
    }
    else
    {
        NRF_LOG_WARNING("nrf_fstorage_erase() failed with error 0x%x.", rc);
    }

    return rc;
}

ret_code_t nrf_dfu_flash_read(uint32_t read_addr, uint32_t length, void *buf)
{
	ret_code_t res;
	
	res = nrf_fstorage_read(&m_fs, read_addr, &buf, length);
	
	return res;
}



/******************************************************************************/
/*                 	  	Address Calculate subroutine		                	  	*/
/******************************************************************************/
uint32_t nrf_dfu_bank0_start_addr(void)
{
    if (SD_PRESENT)
    {
        return ALIGN_TO_PAGE(SD_SIZE_GET(MBR_SIZE));
    }
    else
    {
        return MBR_SIZE;
    }
}


uint32_t nrf_dfu_bank1_start_addr(void)
{
    uint32_t bank0_addr = nrf_dfu_bank0_start_addr();
    return ALIGN_TO_PAGE(bank0_addr + s_dfu_settings.bank_0.image_size);
}


uint32_t nrf_dfu_app_start_address(void)
{
    return nrf_dfu_bank0_start_addr();
}


uint32_t nrf_dfu_softdevice_start_address(void)
{
    return MBR_SIZE;
}

void nrf_dfu_bank_invalidate(nrf_dfu_bank_t * const p_bank)
{
    // Set the bank-code to invalid, and reset size/CRC
    memset(p_bank, 0, sizeof(nrf_dfu_bank_t));

    // Reset write pointer after completed operation
    s_dfu_settings.write_offset = 0;
}
