/**
 * Copyright (c) 2014 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @file
 * @defgroup uart_example_main main.c
 * @{
 * @ingroup uart_example
 * @brief UART Example Application main file.
 *
 * This file contains the source code for a sample application using UART.
 *
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "nrf_delay.h"
#include "nrf.h"
#include "app_error.h"
#include "app_uart.h"
#include "bsp.h"
#include "crc32.h"

#include "Process_Control.h"
#include "Library_InternalErrorTable.h"
#include "Protocol.h"
#include "Protocol_PackageDefine.h"
#include "MemoryOperation.h"
#include "DataCrypto.h"
#include "Communication_UART.h"
#include "Timer.h"
#include "nrf_bootloader_app_start.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

BOOTPROCESS_PROCESS sBootloader;
BOOTPROCESS_FLAG	sBootFlag;
BOOTINFORESERVEAREA sBootInfoArea;
PERIPHERALFLAG sPeripheralFlag;
PERIPHERALDATA sPeripheralData;
ERRORCOUNTER sErrorCounter;

extern PROTOCOLINFORMATION sProtocolInfo;
extern NOTIFYBOOT sNotifyBoot;
extern ACK sACK;
extern ERROR sError;
extern EXIT sExit;

extern uint32_t Timer_BasicTick_Cnt;

/* Enable APPROTECT */
const uint32_t approtect_set __attribute__ ((used, at(0x10001208))) = 0xFFFFFF00;

static uint8_t BootloaderVersion_Ary[2] = { 0 };
static uint8_t BootloaderProtocolVersion_Buf = 0;

typedef struct
{
	uint32_t BasicTime_Begin;
	uint32_t BasicTime_Now;
	uint8_t WorkingTimeout;
}TIMEOUTCOUNTER;
TIMEOUTCOUNTER sTimeout;

static uint8_t Process_TimeoutCount(TIMEOUTCOUNTER *p_Timeout, uint32_t TimerTick);
uint8_t * BootloaderVersionRead(void);
uint8_t BootloaderProtocolVersionRead(void);

/**
 * @brief Function for main application entry.
 */
int main(void)
{
    ret_code_t err_code;
		uint8_t ExaminationResult_Buf = 0;
		uint8_t	tempPackageTransmit_Array[CommandPackageLength] ={ 0 };

		/* nRF Log Initialization */
    err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);
    NRF_LOG_DEFAULT_BACKENDS_INIT();
		
		/* Memory Operation Initialization */
		MemoryOperation_Initialization();
	
    /* UART Initialization */
		UART_Initialization();
	
		/* Timer Initialzation */
		Timer_Initialzation();

		/* BSP Initialization */
		bsp_board_init(BSP_INIT_LEDS);
		
		/* Setting Bootloader Protocol/Firmware Version 2021.11.12 */
		BootloaderVersion_Ary[0] = 0x00;
		BootloaderVersion_Ary[1] = 0x20;
		NRF_LOG_INFO("AWAYSPEED Bluetooth BootLoader Ver %X.%X", BootloaderVersion_Ary[0],
																														 BootloaderVersion_Ary[1]);
		
		BootloaderProtocolVersion_Buf = 5;
		NRF_LOG_INFO("BootLoader Protocol Ver %d", BootloaderProtocolVersion_Buf);
		
		sNotifyBoot.BootFwVer = BootloaderVersion_Ary[1];
		sNotifyBoot.BootProtocolVer = BootloaderProtocolVersion_Buf;

    while(1)
    {
			switch(sBootloader.MainProcess)
			{
				case Connection:
					if(sBootFlag.Connection == TransmitStatus)
					{
						NRF_LOG_INFO("NotifyBoot");
						memcpy(tempPackageTransmit_Array, &sNotifyBoot, CommandPackageLength);
						UARTDataTransimt(tempPackageTransmit_Array, CommandPackageLength);
						
						sBootFlag.Connection = ReceiveStatus;
					}
					else
					{
						uint8_t result = Process_TimeoutCount(&sTimeout, Timer_BasicTick_Cnt);
						switch(result)
						{
							case LIBERRTAB_TERR_TIMEOUT:
								NRF_LOG_INFO("No Connection Request, Back to Application");
								sBootloader.MainProcess = Activation;
								break;
							
							case LIBERRTAB_TERR_TRANSMIT:
								sBootFlag.Connection = TransmitStatus;
								break;
							
							case LIBERRTAB_TERR_WAIT_RECEIVE:
								if(sPeripheralFlag.UARTReceiveFinish)
								{
									sPeripheralFlag.UARTReceiveFinish = 0;
									
#ifdef forUARTDataNRF_LOG
									NRF_LOG_RAW_INFO("Connection -> ");
									for(uint8_t i = 0; i < (CommandPackageLength - UnusedPackageLength); i++)
									{
										NRF_LOG_RAW_INFO(" %02X ", sPeripheralData.cUARTRx[i]);
									}
									NRF_LOG_RAW_INFO("\r\n");
#endif	
									/* Determine Package Size */
									ExaminationResult_Buf = ProtocolExamination(&sBootloader, &sPeripheralData);
									if(ExaminationResult_Buf == LIBERRTAB_SUCCESS)
									{
										/* Memory Operation Process */
										Process_MemoryOperation(&sBootloader);
										
										NRF_LOG_DEBUG("ACK");
										sACK.ContinueTransCnt = 0;
										memcpy(tempPackageTransmit_Array, &sACK, CommandPackageLength);
										UARTDataTransimt(tempPackageTransmit_Array, CommandPackageLength);										
										
										memset(&sTimeout, 0, sizeof(sTimeout));
										sBootloader.MainProcess = ReceiveSetting;
									}
									else
									{
										/* Error Command */
										NRF_LOG_DEBUG("Continue ACK, Error");
										sACK.ContinueTransCnt = 1;
										memcpy(tempPackageTransmit_Array, &sACK, CommandPackageLength);
										UARTDataTransimt(tempPackageTransmit_Array, CommandPackageLength);
										
										sBootloader.PreviousProcess = Connection;
										sBootloader.MainProcess = ErrorProcessing;
										sBootFlag.Connection = TransmitStatus;
									}									
								}
								break;
							
							default:
								break;
						}
					}
					break;
					
				case ReceiveSetting:				
				case FirmwareReceive:
					{
						uint8_t result = Process_TimeoutCount(&sTimeout, Timer_BasicTick_Cnt);
						switch(result)
						{
							case LIBERRTAB_TERR_TIMEOUT:
								ExaminationResult_Buf = LIBERRTAB_TERR_TIMEOUT;
								sBootloader.MainProcess = ErrorProcessing;
								sBootFlag.ErrorProcessing = TransmitStatus;
								break;
						
							case LIBERRTAB_TERR_WAIT_RECEIVE:
								/* Command Receive */
								if(sPeripheralFlag.UARTReceiveFinish)
								{
									sPeripheralFlag.UARTReceiveFinish = 0;
									
#ifdef forUARTDataNRF_LOG
									NRF_LOG_RAW_INFO("Firmware Receive -> ");
									for(uint8_t i = 0; i < (CommandPackageLength - UnusedPackageLength); i++)
									{
										NRF_LOG_RAW_INFO(" %02X ", sPeripheralData.cUARTRx[i]);
									}
									NRF_LOG_RAW_INFO("\r\n");
#endif	
									
									/* Determine Data */
									ExaminationResult_Buf = ProtocolExamination(&sBootloader, &sPeripheralData);
									switch(ExaminationResult_Buf)
									{
										case LIBERRTAB_SUCCESS:
											NRF_LOG_DEBUG("ACK");
											sACK.ContinueTransCnt = 0;
											
											/* Memory Operation Process */
											Process_MemoryOperation(&sBootloader);
											
											/* Import CRC Value */
											if(sProtocolInfo.PackageNumCnt == sProtocolInfo.PackageQuantity)
											{
												sACK.AppFwCRC[0] = (MemoryOperation_AppFwCRC_Read() & 0xFF000000) >> 24;
												sACK.AppFwCRC[1] = (MemoryOperation_AppFwCRC_Read() & 0x00FF0000) >> 16;
												sACK.AppFwCRC[2] = (MemoryOperation_AppFwCRC_Read() & 0x0000FF00) >> 8;
												sACK.AppFwCRC[3] = (MemoryOperation_AppFwCRC_Read() & 0x000000FF);
											}
											
											memcpy(tempPackageTransmit_Array, &sACK, CommandPackageLength);
											UARTDataTransimt(tempPackageTransmit_Array, CommandPackageLength);
										
											memset(&sTimeout, 0, sizeof(sTimeout));
											sBootloader.MainProcess = FirmwareReceive;
											break;
							
										case LIBERRTAB_FINISH:
											NRF_LOG_DEBUG("ACK");
											sACK.ContinueTransCnt = 0;
											memcpy(tempPackageTransmit_Array, &sACK, CommandPackageLength);
											UARTDataTransimt(tempPackageTransmit_Array, CommandPackageLength);
							
											memset(&sTimeout, 0, sizeof(sTimeout));
										
											/* Calculate and Write Bootloader Information Reserve Area */
											MemoryOperation_BootInfo_Calculate();
											MemoryOperation_BootInfo_Write();
										
											/* Activation Application */
											NRF_LOG_INFO("Activation");
											sBootloader.MainProcess = Activation;
											break;
									
										default:
											/* Error Command */
											NRF_LOG_DEBUG("Continue ACK, Error");
											sACK.ContinueTransCnt = 1;
											memcpy(tempPackageTransmit_Array, &sACK, CommandPackageLength);
											UARTDataTransimt(tempPackageTransmit_Array, CommandPackageLength);
										
											if(sBootloader.MainProcess == ReceiveSetting)
												sBootloader.PreviousProcess = ReceiveSetting;
											else
												sBootloader.PreviousProcess = FirmwareReceive;
										
											sBootloader.MainProcess = ErrorProcessing;
											sBootFlag.ErrorProcessing = TransmitStatus;
											break;
									}
									
								}							
								break;
								
							default:							
								break;
						}
					}
					break;
				
				case Activation:
				{
					/* Read Bootloader Information Reserve Area */
					MemoryOperation_BootInfo_Read();
					
					if((sBootInfoArea.AppFwBinSize != 0xFFFFFFFF) &&
						 (sBootInfoArea.AppFwBinCRC != 0xFFFFFFFF))
					{
						/* Calculate Reserved Area CRC */
						uint32_t ReservedAreaCRC_Buf = crc32_compute((uint8_t *)BootReserveAreaAddr, BootReserveAreaLength - 4, NULL);
						NRF_LOG_DEBUG("ReservedAreaCRC_Buf : %X", ReservedAreaCRC_Buf);
						
						/* Calculate Application CRC */
						uint32_t FwCRCValue_Buf = crc32_compute((uint8_t *)FwStartAddress, sBootInfoArea.AppFwBinSize, NULL);
						NRF_LOG_DEBUG("FwCRCValue_Buf : %X", FwCRCValue_Buf);
						
						if((sBootInfoArea.AppFwBinCRC == FwCRCValue_Buf) && 
							 (sBootInfoArea.ReserveAreaCRC == ReservedAreaCRC_Buf))
						{
							NRF_LOG_DEBUG("Bootloader Reserved Area Examination Success \r\n");
							nrf_bootloader_app_start();
						}
						else
						{
							NRF_LOG_DEBUG("Bootloader Reserved Area Examination Fail, Restart bootloader \r\n");
						
							/* All State back to default */
							memset(&sBootloader, 0, sizeof(sBootloader));
							memset(&sBootFlag, 0, sizeof(sBootFlag));
							memset(&sPeripheralFlag, 0, sizeof(sPeripheralFlag));
							memset(&sPeripheralData, 0, sizeof(sPeripheralData));
							memset(&sErrorCounter, 0, sizeof(sErrorCounter));								
						}
					}
					else
					{
						NRF_LOG_DEBUG("Fw CRC Value Read Fail, Restart bootloader \r\n");
						
						/* All State back to default */
						memset(&sBootloader, 0, sizeof(sBootloader));
						memset(&sBootFlag, 0, sizeof(sBootFlag));
						memset(&sPeripheralFlag, 0, sizeof(sPeripheralFlag));
						memset(&sPeripheralData, 0, sizeof(sPeripheralData));
						memset(&sErrorCounter, 0, sizeof(sErrorCounter));						
					}
				}	break;
				
				case ErrorProcessing:
					if(sBootFlag.ErrorProcessing == TransmitStatus)
					{
						NRF_LOG_DEBUG("ErrorProcessing : Error Code");
						switch(ExaminationResult_Buf)
						{
							case LIBERRTAB_GERR_CNS:
								sErrorCounter.UnknowCommand++;
								NRF_LOG_DEBUG("LIBERRTAB_GERR_CNS : %d", sErrorCounter.UnknowCommand);
							
								if(sErrorCounter.UnknowCommand <= Errlimit_UnknowCommand)
								{
									sError.ErrorCode = eUnknowCommand;
									memcpy(tempPackageTransmit_Array, &sError, CommandPackageLength);
									UARTDataTransimt(tempPackageTransmit_Array, CommandPackageLength);
									sBootFlag.ErrorProcessing = ReceiveStatus;
								}
								else
								{
									ExaminationResult_Buf = LIBERRTAB_RESTART;
								}
								
								break;
								
							case LIBERRTAB_GERR_VOOR:
								sErrorCounter.InvalidValue++;
								NRF_LOG_DEBUG("LIBERRTAB_GERR_VOOR : %d", sErrorCounter.InvalidValue);
							
								if(sErrorCounter.InvalidValue <= Errlimit_InvalidValue)
								{
									sError.ErrorCode = eInvalidValue;
									memcpy(tempPackageTransmit_Array, &sError, CommandPackageLength);
									UARTDataTransimt(tempPackageTransmit_Array, CommandPackageLength);
									sBootFlag.ErrorProcessing = ReceiveStatus;
								}
								else
								{
									ExaminationResult_Buf = LIBERRTAB_RESTART;
								}
								break;
								
							case LIBERRTAB_GERR_IPLOIF:
								sErrorCounter.PackageSize++;
								NRF_LOG_DEBUG("LIBERRTAB_GERR_IPLOIF : %d", sErrorCounter.PackageSize);

								if(sErrorCounter.PackageSize <= Errlimit_PackageSize)
								{
									sError.ErrorCode = ePackageSize;
									memcpy(tempPackageTransmit_Array, &sError, CommandPackageLength);
									UARTDataTransimt(tempPackageTransmit_Array, CommandPackageLength);
									sBootFlag.ErrorProcessing = ReceiveStatus;
								}
								else
								{
									ExaminationResult_Buf = LIBERRTAB_RESTART;
								}
								break;
		
							case LIBERRTAB_TERR_TIMEOUT:
								NRF_LOG_DEBUG("LIBERRTAB_TERR_TIMEOUT");
						
							case LIBERRTAB_RESTART:
								NRF_LOG_DEBUG("LIBERRTAB_RESTART");
						
								/* Exit Bootloader */
								sExit.BootStatus = 0x01;
								memcpy(tempPackageTransmit_Array, &sExit, CommandPackageLength);
								UARTDataTransimt(tempPackageTransmit_Array, CommandPackageLength);
						
								sBootloader.MainProcess = Activation;
								break;
							
							case LIBERRTAB_EERR_UPDATEFAILURE:
								NRF_LOG_DEBUG("Application Firmware Update Fail, Restart bootloader \r\n");
						
								/* All State back to default */
								memset(&sBootloader, 0, sizeof(sBootloader));
								memset(&sBootFlag, 0, sizeof(sBootFlag));
								memset(&sPeripheralFlag, 0, sizeof(sPeripheralFlag));
								memset(&sPeripheralData, 0, sizeof(sPeripheralData));
								memset(&sErrorCounter, 0, sizeof(sErrorCounter));	
								break;
							
							default:
								break;
						}
					}
					else
					{
						/* Wait for Host side response ACK */	
						uint8_t result = Process_TimeoutCount(&sTimeout, Timer_BasicTick_Cnt);
						switch(result)
						{
							case LIBERRTAB_TERR_TIMEOUT:
								ExaminationResult_Buf = LIBERRTAB_TERR_TIMEOUT;
								sBootFlag.ErrorProcessing = TransmitStatus;
								break;
							
							case LIBERRTAB_TERR_WAIT_RECEIVE:
								if(sPeripheralFlag.UARTReceiveFinish)
								{
									sPeripheralFlag.UARTReceiveFinish = 0;
									
									/* Determine ACK */
									ExaminationResult_Buf = ProtocolExamination(&sBootloader, &sPeripheralData);
									if(ExaminationResult_Buf == LIBERRTAB_SUCCESS)
									{
										/* Back to the original process, but not clean the basic time */
										sBootloader.MainProcess = sBootloader.PreviousProcess;
										sBootFlag.ErrorProcessing = TransmitStatus;
									}
								}
								break;
							
							default:
								break;
						}						
					}				
					break;
				
				default:
					break;
			}
    }
}


/** @} */

static uint8_t Process_TimeoutCount(TIMEOUTCOUNTER *p_Timeout, uint32_t TimerTick)
{
	uint8_t result;
	
	if(sTimeout.BasicTime_Begin == 0)
	{
		p_Timeout -> BasicTime_Begin = TimerTick;
		result = LIBERRTAB_TERR_BEGIN;
	}
	else
	{
		p_Timeout -> BasicTime_Now = TimerTick;
		if((p_Timeout -> BasicTime_Now - p_Timeout -> BasicTime_Begin) > ErrTimeout_Time)
		{
			p_Timeout -> WorkingTimeout++;
			if(p_Timeout -> WorkingTimeout > ErrTimeout_Cnt)
			{
				NRF_LOG_DEBUG("Timeout");
				memset(&sTimeout, 0, sizeof(sTimeout));
				result = LIBERRTAB_TERR_TIMEOUT;
			}
			else
			{
				p_Timeout -> BasicTime_Begin = 0;
				result = LIBERRTAB_TERR_TRANSMIT;
			}
		}
		else
		{
			result = LIBERRTAB_TERR_WAIT_RECEIVE;
		}
	}
	return result;
}

uint8_t * BootloaderVersionRead(void)
{
	return BootloaderVersion_Ary;
}

uint8_t BootloaderProtocolVersionRead(void)
{
	return BootloaderProtocolVersion_Buf;
}
