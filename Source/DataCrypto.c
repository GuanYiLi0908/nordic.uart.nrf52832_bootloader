

#include "DataCrypto.h"
#include "MemoryOperation.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"


const static uint16_t decryptTable[16] = { 0x0, 0xE, 0x5, 0x9, 0x2, 0x6, 0xB, 0xC, 0xD, 0x4, 0x1, 0xA, 0x8, 0x7, 0x3, 0xF }; 

void CryptoProcess(uint8_t InputBuf[], uint8_t Datalength, uint8_t OutputBuf[])
{	
//	NRF_LOG_DEBUG("CryptoProcess Output :");
	for(uint8_t i = 0; i < Datalength; i++)
	{
		if(i & 0x01)
		{
			OutputBuf[i] = (decryptTable[InputBuf[i] >> 4] << 4) | (InputBuf[i] & 0x0F);
		}
		else
		{
			OutputBuf[i] = (InputBuf[i] & 0xF0) | decryptTable[InputBuf[i] & 0x0F];
		}
//		NRF_LOG_RAW_INFO("%02X ", OutputBuf[i]);
	}
//	NRF_LOG_RAW_INFO("\r\n");
}
