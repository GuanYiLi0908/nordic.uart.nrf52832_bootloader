

#include "Protocol_PackageDefine.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#define Device_BLE 0x42
//#define Device_VCU 0x56
//#define Device_DRV 0x44
NOTIFYBOOT sNotifyBoot = 
{
	.OBD_Header				= ohBluetooth,
	.OBD_Length				= hNotifyBoot,
	.Header 					= hNotifyBoot,
	.CommandType			= ctNotifyBoot,
	.ID								= {0x32, 0x60},
	.DeviceType				= Device_BLE,
	.UserReserve			= {0x00, 0x00},
	.BootProtocolVer 	= 0x00,
	.BootFwVer				= 0x00
};

ACK sACK = 
{
	.OBD_Header				= ohBluetooth,
	.OBD_Length				= hResponse,
	.Header						= hResponse,
	.CommandType			= ctACK,
	.ContinueTransCnt	= 0x00,
};

ERROR sError = 
{
	.OBD_Header				= ohBluetooth,
	.OBD_Length				= hResponse,
	.Header						= hResponse,
	.CommandType			= ctError,
	.ErrorCode				= 0x00,
};

EXIT sExit = 
{
	.OBD_Header				= ohBluetooth,
	.OBD_Length				= hRequset,
	.Header						= hRequset,
	.CommandType			= ctExit,
	.BootStatus				= 0x00,
	.Time							= {0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
};
