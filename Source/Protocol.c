
#include "Library_InternalErrorTable.h"
#include "Protocol.h"
#include "MemoryOperation.h"
#include "Communication_UART.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

PROTOCOLINFORMATION sProtocolInfo;
extern PERIPHERALFLAG sPeripheralFlag;

static uint8_t AppFwUpdateTime_Ary[6] = { 0 };

/******************************************************************************/
/*                 	  		Protocol Package subroutine		                	  	*/
/******************************************************************************/
uint8_t ProtocolExamination(BOOTPROCESS_PROCESS *p_Process, PERIPHERALDATA *p_Data)
{
	int res = LIBERRTAB_GERR_CNS;
	uint8_t tempCompareTest_Array[PackageHeaderCompareLength] = { 0 };
	
	memcpy(tempCompareTest_Array, p_Data->cUARTRx, PackageHeaderCompareLength);
	
	switch(p_Process -> MainProcess)
	{
		case Connection:
			{
				uint8_t tempCompareCtrl_Array[PackageHeaderCompareLength] = VCU_ReqHeader;
				if(PackageHeaderCompare(tempCompareCtrl_Array, tempCompareTest_Array, PackageHeaderCompareLength) == 0)
				{
					/* Determine PackageSize */
					switch(p_Data -> cUARTRx[3])
					{
						case ctPackageSize:
							NRF_LOG_INFO("Receive Package Size");
							/* Package Size */
							sProtocolInfo.PackageSize = ((p_Data->cUARTRx[4] << 16) | 
																					 (p_Data->cUARTRx[5] << 8)  | 
																			     (p_Data->cUARTRx[6]));
							NRF_LOG_DEBUG("PackageSize : %d ", sProtocolInfo.PackageSize);
						
							sProtocolInfo.TotalBinSize = ((p_Data->cUARTRx[7] << 24) | 
																						(p_Data->cUARTRx[8] << 16) | 
																						(p_Data->cUARTRx[9] << 8)	 |
																						(p_Data->cUARTRx[10]));
							NRF_LOG_DEBUG("TotalBinSize : %d ", sProtocolInfo.TotalBinSize);
						
							if((sProtocolInfo.PackageSize == 0 ) || 
								 (sProtocolInfo.TotalBinSize == 0))
							{
								res = LIBERRTAB_GERR_VOOR;
							}
							else if(sProtocolInfo.PackageSize > UART_RX_BUF_SIZE)
							{
								res = LIBERRTAB_GERR_IPLOIF;
							}
							else
							{
								sProtocolInfo.PackageQuantity = sProtocolInfo.TotalBinSize / sProtocolInfo.PackageSize;
								if((sProtocolInfo.TotalBinSize % sProtocolInfo.PackageSize) != 0)
									sProtocolInfo.PackageQuantity = sProtocolInfo.PackageQuantity + 1;
								NRF_LOG_DEBUG("PackageNumber : %d", sProtocolInfo.PackageQuantity); 
								
								res = LIBERRTAB_SUCCESS;
							}
							//PROCESS_ERR_MEM_FULL
							break;
						
						default:
							res = LIBERRTAB_GERR_CNS;
							break;
					 }
				}
			}
			break;
			
		case ReceiveSetting:
			{
				uint8_t tempCompareCtrl_Array[PackageHeaderCompareLength] = VCU_ReqHeader;
				if(PackageHeaderCompare(tempCompareCtrl_Array, tempCompareTest_Array, PackageHeaderCompareLength) == 0)
				{				
					if(p_Data -> cUARTRx[3] == ctStartAndSetting)
					{
						NRF_LOG_INFO("Receive Start and Setting");
						res = LIBERRTAB_SUCCESS;
					}
				}
			}
			break;
				
		case FirmwareReceive:
			if(sProtocolInfo.PackageNumCnt != sProtocolInfo.PackageQuantity)
			{
				uint8_t tempCompareCtrl_Array[PackageHeaderCompareLength] = VCU_DataHeader;
				if(PackageHeaderCompare(tempCompareCtrl_Array, tempCompareTest_Array, PackageHeaderCompareLength) == 0)
				{
					sProtocolInfo.PackageNumCnt++;
					NRF_LOG_DEBUG("DataPackage Counter : %d / %d", sProtocolInfo.PackageNumCnt, sProtocolInfo.PackageQuantity);
						
#ifdef forUARTDataNRF_LOG
					NRF_LOG_RAW_INFO("Bin :");
					for(uint8_t i = 0; i < (CommandPackageLength - UnusedPackageLength); i++)
					{
						NRF_LOG_RAW_INFO(" %02X ", p_Data -> cUARTRx[i]);
					}
					NRF_LOG_RAW_INFO("\r\n");
#endif	
					
					res = LIBERRTAB_SUCCESS;
				}
			}
			else
			{
				uint8_t tempCompareCtrl_Array[PackageHeaderCompareLength] = VCU_ReqHeader;
				if(PackageHeaderCompare(tempCompareCtrl_Array, tempCompareTest_Array, PackageHeaderCompareLength) == 0)
				{
					if(p_Data -> cUARTRx[3] == ctExit)
					{
						NRF_LOG_INFO("Receive Exit : %X", p_Data -> cUARTRx[4]);
						
						switch(p_Data -> cUARTRx[4])
						{
							case 0:
							{
								memcpy(AppFwUpdateTime_Ary, p_Data -> cUARTRx + 4, 6);
						
								NRF_LOG_DEBUG("Application Firmware Update Time : ");
								for(uint8_t i = 0; i < 6; i++)
								{
									NRF_LOG_RAW_INFO(" %X ", AppFwUpdateTime_Ary[i]);
								}
								NRF_LOG_RAW_INFO("\r\n");
						
								res = LIBERRTAB_FINISH;
							} break;
							
							case 1:
							{
								NRF_LOG_ERROR("Restart Bootloader");
								res = LIBERRTAB_EERR_UPDATEFAILURE;
							}	break;
						}
					}
				}				
			}
			break;
				
		case Activation:
			break;
				
		case ErrorProcessing:
			{
				uint8_t tempCompareCtrl_Array[PackageHeaderCompareLength] = VCU_RspHeader;
				if(PackageHeaderCompare(tempCompareCtrl_Array, tempCompareTest_Array, PackageHeaderCompareLength) == 0)
				{
					if(p_Data ->cUARTRx[3] == ctACK)
					{
						NRF_LOG_DEBUG("Receive ACK");
						res = LIBERRTAB_SUCCESS;
					}
				}
			}
			break;
	}
	
	return res;
}

static uint8_t PackageHeaderCompare(uint8_t *ControlGroup, uint8_t *TestGroup, uint8_t length)
{
	uint8_t result = 0;
	
	for(uint8_t i = 0; i < length; i++)
	{
		if((ControlGroup[i] - TestGroup[i]) != 0)
		{
			result = result + 1;
		}
	}
	return result;
}

uint8_t * Protocol_AppFwUpdateTime_Read(void)
{
		return AppFwUpdateTime_Ary;
}
