

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "bsp.h"

#include "Communication_UART.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

extern PERIPHERALFLAG sPeripheralFlag;
extern PERIPHERALDATA sPeripheralData;

static UARTRECEIVE sUARTReceive;

/******************************************************************************/
/*                 	  	UART Function Initialization		                	  	*/
/******************************************************************************/
/**@brief Function for initializing the UART. */
void UART_Initialization(void)
{
    ret_code_t err_code;

    app_uart_comm_params_t const comm_params =
    {
        .rx_pin_no    = RX_PIN_NUMBER,
        .tx_pin_no    = TX_PIN_NUMBER,
        .rts_pin_no   = RTS_PIN_NUMBER,
        .cts_pin_no   = CTS_PIN_NUMBER,
        .flow_control = APP_UART_FLOW_CONTROL_DISABLED,
        .use_parity   = false,
        .baud_rate    = UART_BAUDRATE_BAUDRATE_Baud115200
    };

    APP_UART_FIFO_INIT(&comm_params,
                       UART_RX_BUF_SIZE,
                       UART_TX_BUF_SIZE,
                       UARTEvent_handler,
                       APP_IRQ_PRIORITY_LOWEST,
                       err_code);

    APP_ERROR_CHECK(err_code);
}

/**@brief   Function for handling app_uart events.
 *
 * @details This function receives a single character from the app_uart module and appends it to
 *          a string. The string is sent over BLE when the last character received is a
 *          'new line' '\n' (hex 0x0A) or if the string reaches the maximum data length.
 */
static void UARTEvent_handler(app_uart_evt_t * p_event)
{
    ret_code_t err_code;

    switch (p_event->evt_type)
    {
        case APP_UART_DATA_READY:					
						UNUSED_VARIABLE(app_uart_get(&sUARTReceive.Data[sUARTReceive.ByteCounter]));
						sUARTReceive.ByteCounter++;
				
						if(sUARTReceive.ByteCounter >= (CommandPackageLength - UnusedPackageLength))
						{
							memcpy(sPeripheralData.cUARTRx, sUARTReceive.Data, sUARTReceive.ByteCounter );
							
							sPeripheralFlag.UARTReceiveFinish = 1;
							sUARTReceive.ByteCounter = 0;
						}
							
            break;

        case APP_UART_COMMUNICATION_ERROR:
            APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_FIFO_ERROR:
            APP_ERROR_HANDLER(p_event->data.error_code);
            break;

        default:
            break;
    }
}

/******************************************************************************/
/*                 	  			UART Function subroutine		                	  	*/
/******************************************************************************/
void UARTDataTransimt(uint8_t *p_Data, uint16_t DataLength)
{
	
#ifdef forUARTDataNRF_LOG	
	NRF_LOG_RAW_INFO("UARTDataTransmit :");
	for(uint16_t i = 0; i < DataLength; i++)
	{
		NRF_LOG_RAW_INFO(" %02X ", p_Data[i]);
	}
	NRF_LOG_RAW_INFO("\r\n");
#endif
	
	for(uint16_t i = 0; i < DataLength; i++)
	{
		app_uart_put(p_Data[i]);
	}
	
	memset(p_Data, 0, DataLength);
}
